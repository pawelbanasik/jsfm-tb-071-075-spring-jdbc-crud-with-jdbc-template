package com.pawelbanasik.springdemo;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pawelbanasik.springdemo.dao.OrganizationDao;
import com.pawelbanasik.springdemo.domain.Organization;

public class JdbcTemplateClassicApp1 {

	public static void main(String[] args) {

		// creating the application context
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans-cp.xml");

		// Create the bean
		OrganizationDao dao = (OrganizationDao) ctx.getBean("orgDao");
		 List<Organization> orgs = dao.getAllOrganizations();
		for (Organization org : orgs) {
			System.out.println(org);
		}

		// close the application context
		((ClassPathXmlApplicationContext) ctx).close();

	}

}
